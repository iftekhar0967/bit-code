@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                 <b>Report (Top Customers by product)</b>
              </div>
              <div class="card-body">
                  <table class="table table-sm table-hover">
                      <tr>
                          <th>{{ __('SL') }}</th>
                          <th>{{ __('Product Name') }}</th>
                          <th>{{ __('Customer Name') }}</th>
                          <th>{{ __('Quantity') }}</th>
                          <th class="text-right">{{ __('Price') }}</th>
                          <th class="text-right">{{ __('Total') }}</th>
                      </tr>

                      @php
                      $unitPrice = 0;
                      $TotalPrice = 0;
                      @endphp
                      @foreach($orderItems as $key => $orderItem)
                          <tr>
                              <td>{{ $key+1 }}</td>
                              <td>{{ $orderItem->product_name }}</td>
                              <td>{{ $orderItem->customer->name ?? '' }}</td>
                              <td>{{ $orderItem->product_qty }}</td>
                              <td class="text-right">{{ $orderItem->product_price }}</td>
                              <td class="text-right">{{ $orderItem->order->total_price ?? '00.00' }}</td>
                          </tr>
                          @php
                          $unitPrice += $orderItem->product_price;
                          $TotalPrice +=$orderItem->order->total_price;
                          @endphp
                      @endforeach

                          <tr>
                              <td colspan="5" class="text-right">{{ $unitPrice}}</td>
                              <td  class="text-right">{{$TotalPrice}}</td>
                          </tr>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
