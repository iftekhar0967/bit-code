@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <a href="{{ route('task.report') }}" class="btn btn-dark">Generate Report</a>
              </div>
          </div>
      </div>

  </div>
</div>
@endsection
