@extends('layouts.app')

@section('content')



<div class="container">
    <h1>Board Lists</h1>
  <form action="{{ route('card.store') }}" method="post">
    @csrf
    <div class="form-group">
      <label for="name">Card Name</label>
      <input type="text" class="form-control" name="name" id="name" placeholder="Enter card name" required>
    </div>
    <input type="hidden" name="id" value={{$id}}>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection
