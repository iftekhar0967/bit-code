@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{ route('form.store') }}" method="post">
        @csrf

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="form-group">
            <label for="consumer_key">Consumer Key</label>
            <div class="row">
                <div class="col-md-4">

                    <input type="text" class="form-control" name="consumer_key" id="consumer_key"
                        placeholder="Enter consumer key" required>
                </div>
                <div class="col-md-4">
                    <a href="https://trello.com/app-key"  target="_blank">Get Your Api Key</a>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
