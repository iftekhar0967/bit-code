@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Board Lists</div>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a type="button" class="btn btn-primary" href="{{route('list.create',$id)}}">Create New List</a>
                    <div class="card-body">
                        <div class="table-responsive">
                            <ul class="list-group">
                                @foreach ($responseBody as $lists)
                                <a href="{{route('list.show',$id=$lists->id)}}">
                                    <li class="list-group-item">{{$lists->name}}</li>
                                </a>
                                @endforeach
                              </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
