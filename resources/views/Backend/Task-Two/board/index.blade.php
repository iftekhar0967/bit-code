@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Boards in this organization</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a type="button" class="btn btn-primary" href="{{route('board.create',$id)}}">Create New Board</a>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($responseBody as $board)
                                        <tr>

                                            <td>
                                                {{ $board->name }}
                                            </td>
                                            <td nowrap="nowrap">

                                                    <a href="{{ route('board.show', $board->id) }}" class="btn btn-icon btn-light btn-hover-primary btn-sm mr-2">
                                                        <i class="fas fa-eye"></i> <input type="hidden" value="{{$id}}">
                                                    </a>

                                                    <a href="{{ route('board.edit', $board->id) }}" class="btn btn-icon btn-light btn-hover-primary btn-sm mr-2">
                                                        <i class="fas fa-edit"></i>
                                                    </a>

                                                    <form method="post" action="{{ route('board.destroy', $board->id) }}" class="d-inline-block">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-icon btn-light btn-hover-primary btn-sm">
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                    </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
