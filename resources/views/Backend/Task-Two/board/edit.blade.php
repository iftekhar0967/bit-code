@extends('layouts.app')

@section('content')
<div class="container">

    <form class="form" id="kt_form" method="post" action="{{ route('board.update', $board->id) }}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Board Name</label>
            <input type="text" class="form-control" value="{{ old('name', $board->name) }}" name="name" id="name"
                placeholder="Enter board name" required>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" value="{{ old('description', $board->desc) }}" name="description" id="description" placeholder="Description"
                required>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

</div>




@endsection
