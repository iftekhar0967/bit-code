@extends('layouts.app')

@section('content')

<div class="container">
  <form action="{{ route('board.store') }}" method="post">
    @csrf
    <div class="form-group">
      <label for="name">Board Name</label>
      <input type="text" class="form-control" name="name" id="name" placeholder="Enter board name" required>
    </div>
    <div class="form-group">
      <label for="description">Description</label>
      <input type="text" class="form-control" name="description" id="description" placeholder="Description" required>
    </div>
    <input type="hidden" name="id" value={{$id}}>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection
