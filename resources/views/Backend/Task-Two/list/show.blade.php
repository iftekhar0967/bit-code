@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Board Lists</div>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a type="button" class="btn btn-primary" href="{{route('card.create',$id)}}">Create New Card</a>
                    <div class="card-body">
                        <div class="table-responsive">
                            <ul class="list-group">
                                @foreach ($responseBody as $cards)
                                    <li class="list-group-item">{{$cards->name}}</li>
                                @endforeach
                              </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
