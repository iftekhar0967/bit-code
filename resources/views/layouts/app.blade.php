<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
<<<<<<< HEAD
        <nav class="navbar navbar-expand-sm bg-white border-b border-gray-100">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ route('task') }}">bit-code</a>
=======
        <nav class="navbar navbar-expand-sm navbar-light bg-white shadow-sm">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">{{__('Tasks')}}</a>
>>>>>>> 1c69e8b (task for complete)
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
<<<<<<< HEAD
                            <a class="nav-link" target="blank" href="{{route('form.create')}}">Authorize</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('organization.index')}}">Organizations</a>
=======
                            <a class="nav-link" target="blank" href="{{route('form.create')}}">{{ __('Authorize') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('organization.index')}}">{{ __('Organizations') }}</a>
>>>>>>> 1c69e8b (task for complete)
                        </li>
                    </ul>
                </div>

                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }}
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>

            </div>
        </nav>

        <main class="py-4">
            @include('flash-message')
            @yield('content')
        </main>
    </div>
    <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>
