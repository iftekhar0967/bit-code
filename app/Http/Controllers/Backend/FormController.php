<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Form;
use Illuminate\Http\Request;

class FormController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Backend.Task-Two.form.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'consumer_key' => ['required', 'string'],
        ]);

        $form=Form::create($validated);

        return redirect()->route('form.edit',$form->id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form=Form::findOrFail($id);
        return view('Backend.Task-Two.form.createToken',compact(['id','form']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form=Form::findOrFail($id);
        $validated = $request->validate([
            'token'   => ['required', 'string']
        ]);
        $form->update($validated);
        return redirect()->route('organization.index')->with('success','Item created successfully!');
    }

}
