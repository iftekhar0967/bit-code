<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Form;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ListController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        // dd($id);
        return view('Backend.Task-Two.list.create',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $form = Form::first();

        $queries= http_build_query([
            'name' => $request->name,
            'idBoard' => $id,
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/lists?" . $queries;
        // dd($url);

        $client->request('POST', $url, [
            'verify'  => false,
        ]);

        return redirect()->route('board.show',$id)->with('success','List created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = Form::first();

        $queries= http_build_query([
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/lists/" .$id. "/cards?" . $queries;

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);


        $responseBody = json_decode($response->getBody());

        return view('Backend.Task-Two.list.show',compact('responseBody','id'));
    }



}
