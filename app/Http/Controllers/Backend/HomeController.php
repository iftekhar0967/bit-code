<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function task()
    {
        return view('task');
    }

    public function taskOne()
    {
        return view('Backend.Task-One.task-1');
    }
    public function report()
    {
        $response = Http::get('https://raw.githubusercontent.com/Bit-Code-Technologies/mockapi/main/purchase.json');

        $product_purchase = json_decode($response);

        foreach($product_purchase as $pp){

            // customer data store

            $customer = Customer::updateOrCreate([
                'name' => $pp->name,
                'phone' => $pp->user_phone,
            ]);

            if(!$customer){
                dd('Customer Not Exits Enter Customer Data');
            }

            //check order no

            $checkOrderNo = Order::query()->where('order_no', $pp->order_no)->exists();

            if(!$checkOrderNo){

                // Order

                $order = Order::create([
                    'customer_id' => $customer->id,
                    'order_no' => $pp->order_no,
                    'total_price' => $pp->purchase_quantity * $pp->product_price,
                    'total_qty' => $pp->purchase_quantity,
                ]);

                // Order Items

                $orderItem = OrderItem::create([
                    'order_no' => $pp->order_no,
                    'customer_id' => $customer->id,
                    'product_name' => $pp->product_name,
                    'product_code' => $pp->product_code,
                    'product_qty' => $pp->purchase_quantity,
                    'product_price' => $pp->product_price,
                ]);


            }
        }

       $orderItems = OrderItem::query()
            ->leftJoin('orders','order_items.order_no','=','orders.order_no')
            ->selectRaw('order_items.*, COALESCE(sum(orders.total_qty),0) total')
            ->groupBy('order_items.id')
            ->orderBy('total','desc')
            ->get();

        return view('Backend.Task-One.report', compact('orderItems'));
    }

}
