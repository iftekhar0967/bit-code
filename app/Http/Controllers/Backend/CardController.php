<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Form;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function create($id)
    {
        return view('Backend.Task-Two.card.create',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $form = Form::first();

        $queries= http_build_query([
            'name' => $request->name,
            'idList' => $id,
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/cards?" . $queries;

        $client->request('POST', $url, [
            'verify'  => false,
        ]);

        return redirect()->route('list.show',$id)->with('success','Card created successfully!');
    }

}
