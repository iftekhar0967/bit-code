<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Form;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form = Form::first();

        $queries= http_build_query([
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/members/me/organizations?" . $queries;

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        $responseBody = json_decode($response->getBody());

        return view('Backend.Task-Two.organization.index', compact('responseBody'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = Form::first();

        $queries= http_build_query([
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/organizations/".$id."/boards?" . $queries;

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        $responseBody = json_decode($response->getBody());



        return view('Backend.Task-Two.board.index', compact('responseBody','id'));
    }

}
