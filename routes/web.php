<?php

use App\Http\Controllers\Backend\BoardController;
use App\Http\Controllers\Backend\CardController;
use App\Http\Controllers\Backend\FormController;
use App\Http\Controllers\Backend\HomeController;
use App\Http\Controllers\Backend\ListController;
use App\Http\Controllers\Backend\OrganizationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::prefix('admin')->middleware(['auth', 'verified'])->group(function () {

<<<<<<< HEAD
    Route::get('task', [HomeController::class, 'task'])->name('task');
    Route::get('task-2', [HomeController::class, 'index'])->name('home');
=======
    // Task List

    Route::get('task', [HomeController::class, 'task'])->name('task');

    // Task One

    Route::get('task-one', [HomeController::class, 'taskOne'])->name('task.one');
    Route::get('report', [HomeController::class, 'report'])->name('task.report');


    // Task Two

    Route::get('task-two', [HomeController::class, 'index'])->name('home');
>>>>>>> 1c69e8b (task for complete)

    Route::resource('organization', OrganizationController::class);
    Route::resource('board', BoardController::class)->except('create');
    Route::resource('form', FormController::class);
    Route::resource('list', ListController::class)->except('create');

    Route::get('/board/{id}/create', [BoardController::class, 'create'])->name('board.create');

    Route::get('/list/{id}/create', [ListController::class, 'create'])->name('list.create');

    Route::get('/card/{id}/create', [CardController::class, 'create'])->name('card.create');

    Route::post('/card',[CardController::class,'store'])->name('card.store');
});



